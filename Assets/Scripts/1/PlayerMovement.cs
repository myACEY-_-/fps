﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : Damageable
{
    /*
    CharacterController controller;
    public Transform playerCam;
    public Transform orientation;
    Rigidbody rb;

    /// <summary>
    /// All stats
    /// </summary>
    public float speed = 12f;
    public float jumpHeight = 3f;
    public float gravity = -9.81f;
    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;
    bool isGrounded;
    int jumpNumber; //if 2- you cant jump

    Vector3 velocity;
    /// <summary>
    /// Camera
    /// </summary>
    public float mouseSensivity = 100f;
    float xRotation = 0;




    void Start()
    {
        controller = GetComponent<CharacterController>();
        rb = GetComponent<Rigidbody>();

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        Movement();
        //CheckForWall();
        //WallRunInput();
        HandleCamera();
    }

    void Movement()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        if (isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
            jumpNumber = 0;
        }

        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");
        Vector3 move = transform.right * x + transform.forward * z;
        controller.Move(move * speed * Time.deltaTime);
        if (Input.GetButtonDown("Jump")/* && jumpNumber != 2)
        {
            Jump();
        }
        velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime);
    }

    void HandleCamera()
    {
        float mouseX = Input.GetAxis("Mouse X") * mouseSensivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensivity * Time.deltaTime;

        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90);
        //playerCam.transform.localRotation = Quaternion.Euler(xRotation, 0, cameraTilt);
        transform.Rotate(Vector3.up * mouseX);

        //change cameraTlt for rotrate camera while wall run
        if (Mathf.Abs(cameraTilt) < maxCameraTilt && isWallRunning && isWallRight)
            cameraTilt += Time.deltaTime * maxWallRunSpeed * cameraRotationSpeed;
        if (Mathf.Abs(cameraTilt) < maxCameraTilt && isWallRunning && isWallLeft)
            cameraTilt -= Time.deltaTime * maxWallRunSpeed * cameraRotationSpeed;

        //Tilt camera back
        if (cameraTilt > 0 && !isWallRight && !isWallLeft)
            cameraTilt -= Time.deltaTime * maxCameraTilt * cameraRotationSpeed;
        if (cameraTilt < 0 && !isWallRight && !isWallLeft)
            cameraTilt += Time.deltaTime * maxCameraTilt * cameraRotationSpeed;
    }

    void Jump()
    {
        if (!isWallRunning)
        {
            velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
            jumpNumber += 1;
            print("normalJump");
        }
        else
        {
            print("jump from wall");
            //jump from wall
            if (isWallRight || isWallLeft /*&& Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D)) rb.AddForce(orientation.up * jumpHeight * 5f); //если стена справа ну или слева + я нажал на кнопку для того, чтобы отпрыгнуть, то прыгаем вверх
            if (isWallRight /*&& Input.GetKey(KeyCode.A))
            {
                rb.AddForce(orientation.up * jumpHeight * 3.2f);
                print("wall at right");
            }
            if (isWallLeft /*&& Input.GetKey(KeyCode.D))
            {
                rb.AddForce(orientation.up * jumpHeight * 3.2f);
                print("wall at left");
            }
        }
        //always add force
        rb.AddForce(orientation.forward * jumpHeight * 1f);
    }
*/

    
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    public float damage = 10f;
    public float range = 100f;

    public float fireRate = 15f;
    public Camera fpsCam;

    public float nextTimeToFire = 0f;
    
    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Fire1") && Time.time >= nextTimeToFire)
        {
            nextTimeToFire = Time.time + 1f / fireRate;
            Shoot();
        }        
    }
    void Shoot()
    {
        RaycastHit hit;
        if(Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit, range))
        {
            print(hit.collider.name);
            Damageable enemy = hit.collider.gameObject.GetComponent<Damageable>();
            if(enemy != null && enemy.tag != "Player")
            {
                enemy.TakeDamage(damage);
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Damageable : MonoBehaviour
{
    public float maxHp = 100;
    float curHp;

    public Transform canvas;
    public Text txt;
    public Transform camera;

    private void Awake()
    {
        curHp = maxHp;
    }
    private void Update()
    {
        canvas.LookAt(camera);
    }

    public void TakeDamage(float damage)
    {
        print(damage);
        curHp -= damage;
        txt.text = curHp.ToString();
        if (curHp <= 0)
        {
            Die();
        }
    }

    void Die()
    {
        Destroy(gameObject);
    }
}

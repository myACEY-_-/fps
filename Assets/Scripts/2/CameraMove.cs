﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour
{
    public Transform player; //Head

    private void Update()
    {
        transform.position = player.position;
    }
}
